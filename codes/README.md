# Python index scripts (FOLDER)

* This folder contains the python scripts for memory-indexing scheme we use, in orde to bypass the memory dependencies for each stage. Having two memories, reading and writing occurs to each memory on each butterfly.

# SD cards (FOLDER)

* Contain the available sd-cards produced by Vitis tool for each project. The files of each project can be seen on `vitis_files` folder.

# Vitis files (FOLDER)

* Contain all files used for each Vitis project.

# NTT-multiplier

* myNTT_multiplyer.cpp is for ntt-multiplier project, which contains:
	* NTT_multiplyer top hardware-function
	* NTT_256_hls hardware-function
	* INTT_256_hls hardware-function

# NTT/INTT

* v6.0 is tested on Vitis (SW-Emulation, and Hardware Implementation) and TEST PASSED, in comparison with ntt.c
	* ntt.cpp is for NTT project
	* intt.cpp is for INTT project

## v6.0 modifications made upon v5.0

* rotateLeft had an error (substraction had greater precedence than shifting)
* zeta-indexing of `W[b]` was wrong (needed to be changed, as state `s`iteration is different than the reference)

## v6.1 modifications made upon v6.0

* findParity: change inut to ap_uint<16> to be parametric for greater lengths
* indices `i_e[B]` and `i_o[B]` need to change to ap_uint<16> to be parametric for greater lengths

## v6.11 modifications made upon v6.1

* Removed ap_uint<23> as it was giving missmatches