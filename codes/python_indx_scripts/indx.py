import math
# Python3 code to rotate bits of number

INT_BITS = 4
MAX_NUMBER = pow(2,INT_BITS)-1
# Function to left
# rotate n by d bits
def leftRotate(n, d):

    # In n<<d, last d bits are 0.
    # To put first 3 bits of n at 
    # last, do bitwise or of n<<d
    # with n >>(INT_BITS - d) 
    return (n << d) & MAX_NUMBER|(n >> (INT_BITS - d)) & MAX_NUMBER

# Function to right
# rotate n by d bits
def rightRotate(n, d):

    # In n>>d, first d bits are 0.
    # To put last 3 bits of at 
    # first, do bitwise or of n>>d
    # with n <<(INT_BITS - d) 
    return (n >> d)|(n << (INT_BITS - d)) & MAX_NUMBER

def findParity(n):
    n = (n & 0x000000FF) ^ (n >> 8)
    n = (n & 0x0000000F) ^ (n >> 4)
    n = (n & 0x00000003) ^ (n >> 2)
    n = (n & 0x00000001) ^ (n >> 1)
    return n


# Driver program to test above functions 
print("Example of",INT_BITS,"integer-bits (max-number =",bin(MAX_NUMBER),")")
#test leftRotate() and rightRotate()
n = 1
d = INT_BITS
d2 = INT_BITS-1
print("Left Rotation of",n,"by"
      ,d,"is",end=" ")
print(leftRotate(n, d))

print("Left Rotation of",n,"by"
      ,d2,"is",end=" ")
print(leftRotate(n,d2))

print("Right Rotation of",n,"by"
     ,d,"is",end=" ")
print(rightRotate(n, d))

print("Right Rotation of",n,"by"
     ,d,"is",end=" ")
print(rightRotate(n, d))

#test findParity()
print("parity_array = [",end="")
for i in range(0,pow(2,INT_BITS)):
    print(findParity(i),end=",")
print("]")

print("\nMax parity calculation for 8-bit numbers")
print("Parity of",n,"=",bin(n),"is ",end=" ")
print(findParity(n))
print("Parity of",n+1,"=",bin(n+1),"is ",end=" ")
print(findParity(n+1))
print("Parity of",n+2,"=",bin(n+2),"is ",end=" ")
print(findParity(n+2))
print("Parity of",n+254,"=",bin(n+254),"is ",end=" ")
print(findParity(n+254))
#=======================================================================#

Stages = INT_BITS
States = pow(2,Stages) # = 2^8
lenn = States
print("\n---------------------------- NTT ----------------------------\n")

print("------------------------------------------------------------------")
print("\t\tNTT indexing memory-mapping N=",States);
print("------------------------------------------------------------------")
#(states that are read and write are the same for the new memory-map scheme)
print("\t|ntt.c  |=>|memory-map| memory-map scheme new indices")
print("step    |(s0,s1)|=>|(s0,s1)   |RAM0/1 and index for mem-write/read (same)") 
# Correct NTT structure , correct indices for memory access scheme

for stage in range(0,Stages):
    s = 0;  #state for memory-mapping logic (counter starting from 0)
    group_len = States>>stage+1 #ntt.c index calculation 
    groups = 1<<(stage)         #ntt.c index calculation
    print("Stage = ",stage, "len =",group_len,"groups =",groups)
    print("step ntt.c(s0,s1)=>new(s0,s1)")
    for group in range(1,groups+1):
        start_group = (group-1)*(2*group_len)       #ntt.c index calculation
        for state in range(group_len):
            i_e = leftRotate(s*2,Stages-1-stage)    #new indices (shuffled)
            i_o = leftRotate(s*2+1,Stages-1-stage)  #new indices (shuffled)
            print("S=",start_group+state,"-->(",start_group+state,",",start_group+state+group_len,")=>(",i_e,",",i_o,")",end=" ")
            print("RAM",findParity(i_e),"[",i_e>>1,"]=",i_e," RAM",(findParity(i_e)+1)%2,"[",i_o>>1,"]=",i_o)
            s = s+1; #state for memory-mapping counter + 1

n = int(math.log2(States))
print("-----------------------------------------\n")
print("CORRECT: Paper reference-states of indices, n =",n)
print("(changing the access order of state-groups (s0,s1)")
#implementation of Vitis hls
print("\nStages =",Stages," States =",States)
for stage in range(0,Stages):
    print("Stage =",stage)
    for state in range(States>>1):
        e_tmp = leftRotate(2*state,Stages-1-stage)
        o_tmp = leftRotate(2*state+1,Stages-1-stage)
        i_w_tmp = (1<<stage) + (state % (1<<stage))
        if(findParity(e_tmp) == 1):
            print("(",e_tmp,",",o_tmp,") --> (RAM1,RAM0)=>(",e_tmp>>1,",",o_tmp>>1,") \tz[",i_w_tmp,"]")
        else:
            print("(",e_tmp,",",o_tmp,") --> (RAM0,RAM1)=>(",e_tmp>>1,",",o_tmp>>1,") \tz[",i_w_tmp,"]")

print("\n---------------------------- INTT ----------------------------\n")

print("------------------------------------------------------------------")
print("\t\tINTT indexing memory-mapping N=",States);
print("------------------------------------------------------------------")
#(states that are read and write are the same for the new memory-map scheme)
print("\t|ntt.c  |=>|memory-map| memory-map scheme new indices")
print("step    |(s0,s1)|=>|(s0,s1)   |RAM0/1 and index for mem-write/read (same)") 
# Correct INTT structure , correct indices for memory access scheme
for stage in range(Stages-1,-1,-1):
    s = 0;  #state for memory-mapping logic
    group_len = States>>stage+1
    groups = 1<<(stage)
    print("Stage = ",stage, "len =",group_len,"groups =",groups)
    #print("stage =",stage,"len =",lenn, "number_of_groups = ",groups,"group_length =",group_len)
    for group in range(1,groups+1):
        #print("group =",group)
        start_group = (group-1)*(2*group_len)
        #print("group_start = ",start_group)
        for state in range(group_len):
            i_e = leftRotate(s*2,Stages-1-stage)
            i_o = leftRotate(s*2+1,Stages-1-stage)
            print("S=",start_group+state,"-->(",start_group+state,",",start_group+state+group_len,")=>(",i_e,",",i_o,")",end=" ")
            print("RAM",findParity(i_e),"[",i_e>>1,"]=",i_e," RAM",(findParity(i_e)+1)%2,"[",i_o>>1,"]=",i_o)
            s = s+1;


#new implementation for inverse
print("------------------------------------------------------------------")
print("New INTT memory-map (changing the access order of state-groups (s0,s1)")
print("------------------------------------------------------------------")
z_sum = 1;
print("\nStages =",Stages," States =",States)
for stage in range(Stages-1,-1,-1):
    print("Stage =",stage)
    for state in range(States>>1):
        e_tmp = leftRotate(2*state,Stages-1-stage)
        o_tmp = leftRotate(2*state+1,Stages-1-stage)
        i_w_tmp = z_sum + (state % (1<<stage))
        #print("1<<stage =",1<<Stages-1-stage,"+ (state % (1<<stage)",(state % (1<<stage)))
        if(findParity(e_tmp) == 1):
            print("(",e_tmp,",",o_tmp,") --> (RAM1,RAM0)=>(",e_tmp>>1,",",o_tmp>>1,") \tz[",i_w_tmp,"]")
        else:
            print("(",e_tmp,",",o_tmp,") --> (RAM0,RAM1)=>(",e_tmp>>1,",",o_tmp>>1,") \tz[",i_w_tmp,"]")
    z_sum = z_sum + (1<<stage);


#new i_w calculation based on inverse state traverse
#new implementation for inverse
print("------------------------------------------------------------------")
print("New INTT memory-map (changing the access order of state-groups (s0,s1)")
print("------------------------------------------------------------------")
print("\nStages =",Stages," States =",States)
for stage in range(Stages-1,-1,-1):
    print("Stage =",stage)
    for state in range((States>>1)-1,-1,-1):
        e_tmp = leftRotate(2*state,Stages-1-stage)
        o_tmp = leftRotate(2*state+1,Stages-1-stage)
        # i_w_tmp = z_sum + (state % (1<<stage))
        i_w_tmp =  (1<<stage) + (state % (1<<stage))
        #print("1<<stage =",1<<Stages-1-stage,"+ (state % (1<<stage)",(state % (1<<stage)))
        if(findParity(e_tmp) == 1):
            print("(",e_tmp,",",o_tmp,") --> (RAM1,RAM0)=>(",e_tmp>>1,",",o_tmp>>1,") \tz[",i_w_tmp,"]")
        else:
            print("(",e_tmp,",",o_tmp,") --> (RAM0,RAM1)=>(",e_tmp>>1,",",o_tmp>>1,") \tz[",i_w_tmp,"]")
    z_sum = z_sum + (1<<stage);