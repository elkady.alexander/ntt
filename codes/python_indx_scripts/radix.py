import math
# Python3 code to rotate bits of number

INT_BITS = 4
MAX_NUMBER = pow(2,INT_BITS)-1
# Function to left
# rotate n by d bits
def leftRotate(n, d):

    # In n<<d, last d bits are 0.
    # To put first 3 bits of n at 
    # last, do bitwise or of n<<d
    # with n >>(INT_BITS - d) 
    return (n << d) & MAX_NUMBER|(n >> (INT_BITS - d)) & MAX_NUMBER

# Function to right
# rotate n by d bits
def rightRotate(n, d):

    # In n>>d, first d bits are 0.
    # To put last 3 bits of at 
    # first, do bitwise or of n>>d
    # with n <<(INT_BITS - d) 
    return (n >> d)|(n << (INT_BITS - d)) & MAX_NUMBER

def findParity(n):
    n = (n & 0x0000000F) ^ (n >> 4)
    n = (n & 0x00000003) ^ (n >> 2)
    n = (n & 0x00000001) ^ (n >> 1)
    return n


# Driver program to test above functions 
print("Example of",INT_BITS,"integer-bits (max-number =",bin(MAX_NUMBER),")")
#test leftRotate() and rightRotate()
n = 1
d = INT_BITS
d2 = INT_BITS-1
print("Left Rotation of",n,"by"
      ,d,"is",end=" ")
print(leftRotate(n, d))

print("Left Rotation of",n,"by"
      ,d2,"is",end=" ")
print(leftRotate(n,d2))

print("Right Rotation of",n,"by"
     ,d,"is",end=" ")
print(rightRotate(n, d))

print("Right Rotation of",n,"by"
     ,d,"is",end=" ")
print(rightRotate(n, d))

#test findParity()
print("\nMax parity calculation for 8-bit numbers")
print("Parity of",n,"=",bin(n),"is ",end=" ")
print(findParity(n))
print("Parity of",n+1,"=",bin(n+1),"is ",end=" ")
print(findParity(n+1))
print("Parity of",n+2,"=",bin(n+2),"is ",end=" ")
print(findParity(n+2))
print("Parity of",n+254,"=",bin(n+254),"is ",end=" ")
print(findParity(n+254))
#=======================================================================#

Stages = INT_BITS
States = pow(2,Stages) # = 2^8
lenn = States

r = 2 #Radix-r implementation
N = States
n = int(math.log2(States))
R = int(math.log(N,r))
print("New Paper reference-states of indices, n =",n, "Raxid-", r, "N = ", N, " = r^R = ", r, "^", R)
print("In-place memory-mapping of each state-pair differs")
# k is the right-index of columnc c
# n is the left-index of columnc c
print("\nInitialize inputs")
for state in range(States>>1):
    print("F_r^0(",state,") = f(", state, ")")
for c in range(n):
    print("\nc =",c, "and_numbers = (", int((pow(2,c)-1)), ",", int((pow(2,c-1)-1)), ")")
    # if (c == 0):
    #     for state in range(States>>1):
    #         print("F_r^0(",state,") = f(", state, ")")
    # elif( c == R-1):
    #     for state in range(States>>1):
    #         print("F_r^R(",state,") = f(", state, ")")
    # else:
    for state in range(States>>1):
        n_current = state>>(c+1) 
        n_previous = state>>c
        k_current = state & int((pow(2,c)-1) )
        k_previous = state & int((pow(2,c-1)-1))
        s_current = (n_current<<(c+1)) + (k_current)
        s_previous = (n_previous<<(c+1)) + (k_previous)
        print("s = ", state, "(",s_current,":",n_current,"&",n_previous,") --> (",s_current,":",n_current,"&",n_previous,")")
print("\nWrite to output buffers if needed (indices are the same)")
for state in range(States>>1):          
    print("F_r^R(",state,") = f(", state, ")")