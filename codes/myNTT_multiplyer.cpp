//NTT and INTT HLS-code for Dilithium
//This code is running for max N=2^16
#define QINV 4236238847
#define Q 8380417
#define Q2 2*Q
#define Q256 256*Q
#define MONT 4193792U // 2^32 % Q
#define N 256
#define Stages 8 //!!log2(N)!!
#define B 2 //need to check for higher B how memory-map works
#define F_const (((unsigned long)MONT*MONT % Q) * (Q-1) % Q) * ((Q-1) >> 8) % Q

#include <stdio.h>
#include "ap_int.h"
//typedef ap_uint<23> uint23_t;
//typedef ap_uint<46> uint46_t;

// Primitive roots of unity
const unsigned int zetas[256] = { 0, 25847, 5771523, 7861508, 237124, 7602457, 7504169, 466468, 1826347, 2353451, 8021166, 6288512, 3119733, 5495562, 3111497, 2680103, 2725464, 1024112, 7300517, 3585928, 7830929, 7260833, 2619752, 6271868, 6262231, 4520680, 6980856, 5102745, 1757237, 8360995, 4010497, 280005, 2706023, 95776, 3077325, 3530437, 6718724, 4788269, 5842901, 3915439, 4519302, 5336701, 3574422, 5512770, 3539968, 8079950, 2348700, 7841118, 6681150, 6736599, 3505694, 4558682, 3507263, 6239768, 6779997, 3699596, 811944, 531354, 954230, 3881043, 3900724, 5823537, 2071892, 5582638, 4450022, 6851714, 4702672, 5339162, 6927966, 3475950, 2176455, 6795196, 7122806, 1939314, 4296819, 7380215, 5190273, 5223087, 4747489, 126922, 3412210, 7396998, 2147896, 2715295, 5412772, 4686924, 7969390, 5903370, 7709315, 7151892, 8357436, 7072248, 7998430, 1349076, 1852771, 6949987, 5037034, 264944, 508951, 3097992, 44288, 7280319, 904516, 3958618, 4656075, 8371839, 1653064, 5130689, 2389356, 8169440, 759969, 7063561, 189548, 4827145, 3159746, 6529015, 5971092, 8202977, 1315589, 1341330, 1285669, 6795489, 7567685, 6940675, 5361315, 4499357, 4751448, 3839961, 2091667, 3407706, 2316500, 3817976, 5037939, 2244091, 5933984, 4817955, 266997, 2434439, 7144689, 3513181, 4860065, 4621053, 7183191, 5187039, 900702, 1859098, 909542, 819034, 495491, 6767243, 8337157, 7857917, 7725090, 5257975, 2031748, 3207046, 4823422, 7855319, 7611795, 4784579, 342297, 286988, 5942594, 4108315, 3437287, 5038140, 1735879, 203044, 2842341, 2691481, 5790267, 1265009, 4055324, 1247620, 2486353, 1595974, 4613401, 1250494, 2635921, 4832145, 5386378, 1869119, 1903435, 7329447, 7047359, 1237275, 5062207, 6950192, 7929317, 1312455, 3306115, 6417775, 7100756, 1917081, 5834105, 7005614, 1500165, 777191, 2235880, 3406031, 7838005, 5548557, 6709241, 6533464, 5796124, 4656147, 594136, 4603424, 6366809, 2432395, 2454455, 8215696, 1957272, 3369112, 185531, 7173032, 5196991, 162844, 1616392, 3014001, 810149, 1652634, 4686184, 6581310, 5341501, 3523897, 3866901, 269760, 2213111, 7404533, 1717735, 472078, 7953734, 1723600, 6577327, 1910376, 6712985, 7276084, 8119771, 4546524, 5441381, 6144432, 7959518, 6094090, 183443, 7403526, 1612842, 4834730, 7826001, 3919660, 8332111, 7018208, 3937738, 1400424, 7534263, 1976782 };
const unsigned int zetas_inv[256] = {6403635, 846154, 6979993, 4442679, 1362209, 48306, 4460757, 554416, 3545687, 6767575, 976891, 8196974, 2286327, 420899, 2235985, 2939036, 3833893, 260646, 1104333, 1667432, 6470041, 1803090, 6656817, 426683, 7908339, 6662682, 975884, 6167306, 8110657, 4513516, 4856520, 3038916, 1799107, 3694233, 6727783, 7570268, 5366416, 6764025, 8217573, 3183426, 1207385, 8194886, 5011305, 6423145, 164721, 5925962, 5948022, 2013608, 3776993, 7786281, 3724270, 2584293, 1846953, 1671176, 2831860, 542412, 4974386, 6144537, 7603226, 6880252, 1374803, 2546312, 6463336, 1279661, 1962642, 5074302, 7067962, 451100, 1430225, 3318210, 7143142, 1333058, 1050970, 6476982, 6511298, 2994039, 3548272, 5744496, 7129923, 3767016, 6784443, 5894064, 7132797, 4325093, 7115408, 2590150, 5688936, 5538076, 8177373, 6644538, 3342277, 4943130, 4272102, 2437823, 8093429, 8038120, 3595838, 768622, 525098, 3556995, 5173371, 6348669, 3122442, 655327, 522500, 43260, 1613174, 7884926, 7561383, 7470875, 6521319, 7479715, 3193378, 1197226, 3759364, 3520352, 4867236, 1235728, 5945978, 8113420, 3562462, 2446433, 6136326, 3342478, 4562441, 6063917, 4972711, 6288750, 4540456, 3628969, 3881060, 3019102, 1439742, 812732, 1584928, 7094748, 7039087, 7064828, 177440, 2409325, 1851402, 5220671, 3553272, 8190869, 1316856, 7620448, 210977, 5991061, 3249728, 6727353, 8578, 3724342, 4421799, 7475901, 1100098, 8336129, 5282425, 7871466, 8115473, 3343383, 1430430, 6527646, 7031341, 381987, 1308169, 22981, 1228525, 671102, 2477047, 411027, 3693493, 2967645, 5665122, 6232521, 983419, 4968207, 8253495, 3632928, 3157330, 3190144, 1000202, 4083598, 6441103, 1257611, 1585221, 6203962, 4904467, 1452451, 3041255, 3677745, 1528703, 3930395, 2797779, 6308525, 2556880, 4479693, 4499374, 7426187, 7849063, 7568473, 4680821, 1600420, 2140649, 4873154, 3821735, 4874723, 1643818, 1699267, 539299, 6031717, 300467, 4840449, 2867647, 4805995, 3043716, 3861115, 4464978, 2537516, 3592148, 1661693, 4849980, 5303092, 8284641, 5674394, 8100412, 4369920, 19422, 6623180, 3277672, 1399561, 3859737, 2118186, 2108549, 5760665, 1119584, 549488, 4794489, 1079900, 7356305, 5654953, 5700314, 5268920, 2884855, 5260684, 2091905, 359251, 6026966, 6554070, 7913949, 876248, 777960, 8143293, 518909, 2608894, 8354570};

void NTT_256_hls(unsigned int* p);

// Montgomery function
static unsigned int montgomery_reduce(unsigned long int a) {
	unsigned long int t;
	t = a * QINV;
	t &= ((long unsigned int)1 << 32) - 1;
	t *= Q;
	t = a + t;
	t >>= 32;
	return t;
}

// Parity function for 16-bit input
static bool findParity(ap_uint<16> x)
{
    // recursively divide the (32–bit) integer into two equal
    // halves and take their XOR until only 1 bit is left
    x = (x & 0x000000FF) ^ (x >> 8);
    x = (x & 0x0000000F) ^ (x >> 4);
    x = (x & 0x00000003) ^ (x >> 2);
    x = (x & 0x00000001) ^ (x >> 1);

    return x & 1;
}

// Rotate left function for integer values
static unsigned int rotateLeft(unsigned int x,unsigned int d)
{
	//int_bits = Stages
	//In n<<d, last d bits are 0.
	//To put first Stages-d bits of n at last, do bitwise or of n<<d
	//with n >>(INT_BITS - d)
	return ( ((x << d) & ((1<<Stages)-1)) | (x >> (Stages - d)) );
}


//NTT_256_hls TEST PASSED in SW-Emulation and Hardware
void NTT_256_hls(unsigned int* p)
{
  //feedback regs for each stage
	ap_uint<16> j[B],k[B],i_w[B]; //idx arrays
	ap_uint<16> i_e[B],i_o[B];
#pragma HLS array_partition variable=j complete
#pragma HLS array_partition variable=k complete
#pragma HLS array_partition variable=i_e complete
#pragma HLS array_partition variable=i_o complete
#pragma HLS array_partition variable=i_w complete

	unsigned int localRegs[N/2][2] ;
//no data dependencies between different loops
#pragma HLS dependence variable=localRegs inter false
//#pragma HLS array_partition variable=localRegs complete
#pragma HLS array_partition variable=localRegs block factor=2 dim=1

	unsigned int U[B],V[B],W[B],E[B],O[B];
//  uint23_t U[B],V[B],W[B],E[B],O[B]; (not working)
	bool parity[B]; //calc arrays
#pragma HLS array_partition variable=U complete
#pragma HLS array_partition variable=V complete
#pragma HLS array_partition variable=W complete
#pragma HLS array_partition variable=E complete
#pragma HLS array_partition variable=O complete

  readInput: for(int i=0;i<N;i++){
	  if(findParity(i) ==1){
		  localRegs[i>>1][1] = p[i];
	  }
	  else{
		  localRegs[i>>1][0] = p[i];
	  }
  }


  //NTT: log2(N) rounds, each round has N/2 PEs in total
  unsigned int u = N/2; unsigned int round_indx = 1;
  STAGE_LOOP: for(int i= 0; i < Stages; i++) {
 		//On each stage: for each butterfy-loop create B PEs
	  BUTTERFLY_LOOP: for(int s = 0; s < u; s = s + B) {
		  #pragma HLS pipeline II=2
		  IDX_CALC_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
			  unsigned int tmp = s+b;
			  i_e[b] = rotateLeft((tmp<<1),Stages-1-i); //low index
			  i_o[b] = rotateLeft((tmp<<1) + 1 ,Stages-1-i); //high index
			  parity[b] = findParity(i_e[b]);
			  i_w[b] = (1<<i) + (tmp % (1<<i));/// (1<<(l-i-1)));
		  }

		  MEM_READ_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
			  if(parity[b] == 1){
				  U[b] = localRegs[i_e[b]>>1][1];
				  V[b] = localRegs[i_o[b]>>1][0];
			  }
			  else{
				  U[b] = localRegs[i_e[b]>>1][0];
				  V[b] = localRegs[i_o[b]>>1][1];
			  }
			  W[b] = zetas[i_w[b]];
		  }

		  OP_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
			  unsigned int t = montgomery_reduce((unsigned long)W[b] * V[b]);
			  O[b] = U[b] + Q2 - t;
			  E[b] = U[b] + t;
		  }

		  MEM_WRITE_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
			  if(parity[b] == 1){
				  localRegs[i_e[b]>>1][1] = E[b];
				  localRegs[i_o[b]>>1][0] = O[b];
			  }
			  else{
				  localRegs[i_e[b]>>1][0] = E[b];
				  localRegs[i_o[b]>>1][1] = O[b];
			  }
		  }
	  }
  }

  //Write final coefficients to output RAM
  writeOutput: for(int i=0;i<N;i++){
	  if(findParity(i) == 0){
		  p[i] = localRegs[i>>1][0];
	  }
	  else{
		  p[i] = localRegs[i>>1][1];
	  }
  }
}

//INTT_256_hls TEST PASSED in SW-Emulation and Hardware
void INTT_256_hls(unsigned int* p)
{
  //feedback regs for each stage
	ap_uint<16> j[B],k[B],i_w[B]; //idx arrays
	ap_uint<16> i_e[B],i_o[B];

#pragma HLS array_partition variable=j complete
#pragma HLS array_partition variable=k complete
#pragma HLS array_partition variable=i_e complete
#pragma HLS array_partition variable=i_o complete
#pragma HLS array_partition variable=i_w complete

	unsigned int localRegs[N/2][2];
//no data dependencies between different loops
#pragma HLS dependence variable=localRegs inter false
//#pragma HLS array_partition variable=localRegs complete
#pragma HLS array_partition variable=localRegs block factor=2 dim=1

//  unsigned int out_mem[N/2][2];
	unsigned int U[B],V[B],W[B],E[B],O[B],parity[B]; //calc arrays
#pragma HLS array_partition variable=U complete
#pragma HLS array_partition variable=V complete
#pragma HLS array_partition variable=W complete
#pragma HLS array_partition variable=E complete
#pragma HLS array_partition variable=O complete

  //Read input and write on RAM based on memory-map scheme
  readInput: for(int i=0;i<N/2;i++){
	  ap_uint<16> e_tmp = rotateLeft(i<<1,0);
	  ap_uint<16> o_tmp = rotateLeft((i<<1)+1,0);
	  if(findParity(e_tmp) == 1){
		  localRegs[e_tmp>>1][1] = p[e_tmp];
		  localRegs[o_tmp>>1][0] = p[o_tmp];
	  }
	  else{
		  localRegs[e_tmp>>1][0] = p[e_tmp];
		  localRegs[o_tmp>>1][1] = p[o_tmp];
	  }
  }

  //INTT: log2(N) rounds, each round has N/2 PEs in total
  unsigned int u = N/2; unsigned int z_index = 0;
  STAGE_LOOP: for(int i= Stages-1; i >= 0; i--) {
  	//On each stage: for each butterfy-loop create B PEs
	  BUTTERFLY_LOOP: for(int s = 0; s < u; s = s + B) {
		  #pragma HLS pipeline II=2
		  IDX_CALC_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  unsigned int tmp = s+b;
			  i_e[b] = rotateLeft((tmp<<1),Stages-1-i); //low index
			  i_o[b] = rotateLeft((tmp<<1) + 1 ,Stages-1-i); //high index
			  parity[b] = findParity(i_e[b]);
			  i_w[b] = z_index + (tmp % (1<<i));
		  }

		  MEM_READ_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  if(parity[b] == 1){
				  U[b] = localRegs[i_e[b]>>1][1];
				  V[b] = localRegs[i_o[b]>>1][0];
			  }
			  else{
				  U[b] = localRegs[i_e[b]>>1][0];
				  V[b] = localRegs[i_o[b]>>1][1];
			  }
			  W[b] = zetas_inv[i_w[b]];
		  }

		  OP_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  E[b] = U[b] + V[b];
			  unsigned int tmp_reg = U[b] + Q256 - V[b];
			  O[b] = montgomery_reduce((unsigned long)W[b] * tmp_reg);
		  }

		  MEM_WRITE_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  if(parity[b] == 0){
				  localRegs[i_e[b]>>1][0] = E[b];
				  localRegs[i_o[b]>>1][1] = O[b];
			  }
			  else{
				  localRegs[i_e[b]>>1][1] = E[b];
				  localRegs[i_o[b]>>1][0] = O[b];
			  }
		  }
	  }
	  //Update z_index on each stage
	  z_index = z_index + (1<<i);
  }

  const unsigned int f = (((unsigned long)MONT*MONT % Q) * (Q-1) % Q) * ((Q-1) >> 8) % Q;
  mont_reduce: for(int i=0;i<N/2;i++){
	  localRegs[i][0] = montgomery_reduce((unsigned long)f*localRegs[i][0]);
	  localRegs[i][1] = montgomery_reduce((unsigned long)f*localRegs[i][1]);
  }

  //Write final coefficients to output RAM
  writeOutput: for(int i=0;i<N;i++){
	  if(findParity(i) == 0){
		  p[i] = localRegs[i>>1][0];
	  }
	  else{
		  p[i] = localRegs[i>>1][1];
	  }
  }
}



extern "C"{

void NTT_multiplyer(unsigned int* a){
	unsigned int localRegs_a[N] ; unsigned int localRegs_b[N] ; unsigned int localRegs_c[N] ;
	#pragma HLS array_partition variable=localRegs_a type=block
	#pragma HLS array_partition variable=localRegs_b type=block
	#pragma HLS array_partition variable=localRegs_c type=block

	#pragma HLS dataflow
	//Write from input to memory
	ReadInput_ab: for (int i = 0; i<2*N; i++){
		if(i<N){
			localRegs_a[i] = a[i];
		}
		else{
			localRegs_b[i] = a[i];
		}
	}

	//Execute NTT for a and b vectors
	// (Add 2 compute-units of NTT in hardware-functions, to run in parallel)
	NTTA:NTT_256_hls(localRegs_a);
	NTTB:NTT_256_hls(localRegs_b);

	//dot multiplication
	dotMultiplication: for(int i=0;i<N;i++){
		#pragma HLS unroll factor=2
		localRegs_c[i] = montgomery_reduce((long unsigned int)localRegs_a[i]*localRegs_b[i]);
	}

	//Execute Inverse-NTT for output c
	InvNTT:INTT_256_hls(localRegs_c);

	//Write to output from memory
	WriteOutput: for (int i = 0; i<N; i++){
		a[i] = localRegs_c[i];
	}
}

}
