# Available projects

## dilithium_mode X

* Contains the dilithium scheme testbench, applying Key-generation then Sign and Verification.

	* Can be run with static-variables (using sign_static.c), bypassing the memory-copies inside the kernels (using dilithium_mult_ocl_2funcs.cpp). Time execution minimized. (**!!!FILES ARE AVAILABLE ONLY FOR MODE 5!!!**)

	* Can be run without static variables (using sign.c), using the traditional way of copying the data to the buffers (using dilithium_mult_ocl.cpp). The time execution is larger, because of buffer-allocation and memory-copies.

## multiplier_mode X

* Contains the code that should replace the SW multiplication inside dilithium (not containg the whole Dilithium scheme)

## dilithium2_mm

* Contains a simple multiplication N x N example. Used this before moving to dilithium, as a beta-project.

## NTT

* Contains the Ntt

## Inverse_NTT

* Contains the Inverse-Ntt

# Vitis Project settings

* Add crypto to build-settings C/C++ Build-->Settings-->GCC Host Linker-->Libraries-->Add library--> Type crypto

* Also add -fpermissive to build-settings C/C++ Build-->Settings-->GCC Host Compiler--> At the end of command

* Also add xlc2.hpp, copy "libs" folder in the top of [xrt] project, by importing to build-settings C/C++ Build-->Settings-->Includes-->GCC Host Compiler-->Include paths (-l)--> Type ""${workspace_loc:${ProjName}/libs/common/includes/xcl2}" (not needed after 2021.2)


# What is need in order to use SW_emulator:

## Change Image to an Image created for SW-Emulation (if not available create it with petalinux):

### petalinux-config

* DTG settings --> Kernel Bootargs --> "earlycon clk_ignore_unused root=/dev/ram rw"
* Image packaging --> INITRAMFS


### FILE:project-spect/meta-user/recipes-bsp/device-tree/files/system-user.dtsi

&amba {
	zyxclmm_drm {
		compatible = "xlnx,zocl";
		status = "okay";
		interrupt-parent = <&axi_intc_0>;
		interrupts = <0  4>, <1  4>, <2  4>, <3  4>,
			     <4  4>, <5  4>, <6  4>, <7  4>,
			     <8  4>, <9  4>, <10 4>, <11 4>,
			     <12 4>, <13 4>, <14 4>, <15 4>,
			     <16 4>, <17 4>, <18 4>, <19 4>,
			     <20 4>, <21 4>, <22 4>, <23 4>,
			     <24 4>, <25 4>, <26 4>, <27 4>,
			     <28 4>, <29 4>, <30 4>, <31 4>;
	};
};

&axi_intc_0 {
      xlnx,kind-of-intr = <0x0>;
      xlnx,num-intr-inputs = <0x20>;
      interrupt-parent = <&gic>;
      interrupts = <0 89 4>;
};

## FILE :project-spec/meta-user/conf/user-rootfsconfig

CONFIG_xrt
CONFIG_xrt-dev
CONFIG_zocl
CONFIG_opencl-clhpp-dev 
CONFIG_opencl-headers-dev
CONFIG_packagegroup-petalinux-opencv


## petalinux-package --boot --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf --pmufw images/linux/pmufw.elf --atf images/linux/bl31.elf --fpga
