//#include <stddef.h>
//#include <stdint.h>
//#include <stdio.h>
//#include "../randombytes.h"
//#include "../sign.h"
//#include "../fips202.h"
//#include "../params.h"			//Dilithium Parameters
//#include <time.h>
//
//#define MLEN 59
//#define NTESTS 1000
//
//int main(void)
//{
//  int DILITHIUM_MODE = 5;
//  printf("DILITHIUM MODE = %d\nRunning hardware = %d\n",DILITHIUM_MODE,run_hardware);
//  size_t i, j;
//  int ret;
//  size_t mlen, smlen;
//  uint8_t b;
//  uint8_t m[MLEN + PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_BYTES];
//  uint8_t m2[MLEN + PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_BYTES];
//  uint8_t sm[MLEN + PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_BYTES];
//  uint8_t pk[PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_PUBLICKEYBYTES];
//  uint8_t sk[PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_SECRETKEYBYTES];
//
//  struct timespec start, end;
//  long int time = 0;
//
//  //MAIN LOOP
//  for(i = 0; i < NTESTS; ++i) {
//    randombytes(m, MLEN);
//
//    timespec_get(&start, TIME_UTC);
//    PQCLEAN_DILITHIUM5_CLEAN_crypto_sign_keypair(pk, sk);
//    PQCLEAN_DILITHIUM5_CLEAN_crypto_sign(sm, &smlen, m, MLEN, sk);
//    ret = PQCLEAN_DILITHIUM5_CLEAN_crypto_sign_open(m2, &mlen, sm, smlen, pk);
//    timespec_get(&end, TIME_UTC);
//    time += ((end.tv_sec-start.tv_sec)*1000000000L) + (end.tv_nsec - start.tv_nsec);
//
//    if(i!=0 && ret) {
//      fprintf(stderr, "Verification failed\n");
//      return -1;
//    }
//    if(i!=0 && smlen != MLEN + PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_BYTES) {
//      fprintf(stderr, "Signed message lengths wrong\n");
//      return -1;
//    }
//    if(i!=0 && mlen != MLEN) {
//      fprintf(stderr, "Message lengths wrong\n");
//      return -1;
//    }
//    if(i!=0){
//		for(j = 0; j < MLEN; ++j) {
//		  if(m2[j] != m[j]) {
//			fprintf(stderr, "Messages don't match\n");
//			return -1;
//		  }
//		}
//    }
//
//    randombytes((uint8_t *)&j, sizeof(j));
//    do {
//      randombytes(&b, 1);
//    } while(!b);
//    sm[j % (MLEN + PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_BYTES)] += b;
//    ret = PQCLEAN_DILITHIUM5_CLEAN_crypto_sign_open(m2, &mlen, sm, smlen, pk);
//    if(!ret) {
//      fprintf(stderr, "Trivial forgeries possible\n");
//      return -1;
//    }
//  }
//
//  // PRINT RESULTS
//  printf("PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_PUBLICKEYBYTES = %d\n", PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_PUBLICKEYBYTES);
//  printf("PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_SECRETKEYBYTES = %d\n", PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_SECRETKEYBYTES);
//  printf("PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_BYTES = %d\n", PQCLEAN_DILITHIUM5_CLEAN_CRYPTO_BYTES);
//  // PRINT Execution Time
//  printf("\n%d Dilithium operations took %ld nanonseconds\n----------------------\n", NTESTS, time);
//  printf("\nAverage Dilithium operations took %ld nanonseconds\n----------------------\n", time/(NTESTS));
//  return 0;
//}
