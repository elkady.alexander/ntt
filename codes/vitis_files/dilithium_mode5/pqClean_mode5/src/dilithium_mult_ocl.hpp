#include "CL/cl.h"

unsigned long dilithium_mult_ocl(polyvecl *polyvec_a,polyvecl mat[K], polyveck *polyvec_out);

unsigned long dilithium_mult_ocl2(polyvecl *polyvec_a,polyvecl mat[K], polyveck *polyvec_out);

//Platform initialization
void initialize_device(const char* kernel_file, const char* kernel_name, \
		cl_context* context, cl_kernel* kernel, cl_command_queue* queue,const int DEBUG);
