# NTT_multiplier

## Setup

* Create the project and inclue files
* Right click on application[xrt], on the explorer tab and navigate on "C/C++ build settings"
	* Navigate on Settings/Libraries and add on Libraries (-l) the "crypto" library
* Navigate on the project_kernels.prj and add the following hardware functions (top right light icon)
	* NTT_multiplyer
	* NTT_256_hls
	* INTT_256_hls
* On the project_hw_link.prj you can add 2 compute units for NTT
* Build your project and download as usual

## Tested on Vitis 2021.2:

### On Hardware and TEST PASSED, in comparison with test_mul.c of Dilithium git-project
### On SW-Emulation gives an error in .xo container

## SW-Emulation

Replace project's Image with the one for sw-emulation

## Hardware Implementation

Replace project's Image with the on from zcu104-base-common
