#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include "../params.h"			//Dilithium Parameters
#include "../randombytes.h"		//Randombytes function
#include "../poly.h"			//Declaration of poly functions and structs
#include "../ntt.h"
#include "CL/cl.h"
#include <time.h>
#include <math.h>

int load_file_to_memory(const char *filename, char **result);

#define NTESTS 10000
//Define number of iterations
#define Iters 1
#define DEBUG 0

void initialize_device(const char* kernel_file, const char* kernel_name, cl_context* context, cl_kernel* kernel, cl_command_queue* queue ){
    //Basic variables
    cl_program program; cl_device_id devices[16]; cl_device_id device_id = 0; cl_int err;
    //Platform initialization
	cl_platform_id platform_id = 0;	cl_uint num_platforms;
    //Get platforms
	err = clGetPlatformIDs(0, NULL, &num_platforms);
	if (err < 0) perror("Couldn't find any platforms.");
	cl_platform_id *platform_ids = (cl_platform_id*)malloc(sizeof(cl_platform_id) * num_platforms);
	clGetPlatformIDs(num_platforms, platform_ids, NULL);
	//Select platform Xilinx
	char cl_platform_vendor[1001];
	for (int i = 0; i < int(num_platforms); i++) {
		err = (cl_int) clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, 1000,
												(void *)cl_platform_vendor, NULL);
		if(strcmp(cl_platform_vendor,"Xilinx") == 0){
			printf("Selected platform %d vendor: %s\n---------------------------------\n", i, cl_platform_vendor);
			platform_id = platform_ids[i];
		}
	}
	//Select device id
	cl_uint num_devices;
	err = (cl_int) clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &num_devices);
	char cl_device_name[1001];
	for(int i=0; i< int(num_devices); i++){
		clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name, 0);
		printf("Device %d : %s\n",i,cl_device_name);
		device_id = devices[i];
	}
	//Create Context
	*context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	//Create Command Queue
	*queue = clCreateCommandQueue(*context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
	//Create program kernel
	cl_int err_code; unsigned char *krnl_bin;
//	program = clCreateProgramWithSource(context, 1, (const char**)&program_buffer, &program_size, &err);
    const size_t krnl_size = load_file_to_memory(kernel_file, (char **) &krnl_bin);
    program = clCreateProgramWithBinary(*context, 1, &device_id, &krnl_size,
                                        (const unsigned char **) &krnl_bin, &err, &err_code);
    if(err<0)perror("Error in kernel creation");
	//Create kernel objects
	*kernel = clCreateKernel(program, kernel_name , &err);
	if(err <0) perror("Error initializing kernel");
	size_t wg_size;
	err = clGetKernelWorkGroupInfo(*kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE,	sizeof(wg_size), &wg_size, NULL);
	printf("Kernel workgroup size = %d\n",int(wg_size));
	//release memory from objects
	clReleaseDevice(device_id);
	clReleaseProgram(program);
	free(platform_ids);
}

int main(void) {
    struct timespec start, end, start_cpu, end_cpu;
    const char *kernel_name = "NTT_multiplyer";
    const char *kernel_file = "NTT_multiplyer.xclbin";

    //Basic variables
    cl_context context;
    cl_kernel kernel; cl_command_queue queue; cl_int err;

    //Platform initialization
    initialize_device(kernel_file,kernel_name,&context,&kernel,&queue);
	/*---------------------------------------------------------------------------------
					Run for 1) C-code and 2) HW-device
	---------------------------------------------------------------------------------*/
    uint8_t seed[SEEDBYTES]; uint16_t nonce = 0;
    polyvecNum noCL_a, noCL_b;							//input vector of polynomials
    polyvecNum noCL_c;//, withCL_c; 					//output vector of polynomials
    polyvecNum2 withCL_a;
    cl_ulong time; long int timeCPU =0; long int timeOCL =0; long int timeOCL_kernel =0;

    printf("\nSize of polynomial: %d \n", polyNum);

    int tv_missmatch = 0; randombytes(seed, sizeof(seed));
    for (int test = 0; test < NTESTS; ++test) {
    	// Create test-vectors
        for (int i = 0; i < polyNum; ++i) {
            poly_uniform(&noCL_a.vec[i], seed, nonce++);
            poly_uniform(&noCL_b.vec[i], seed, nonce++);
            memcpy(&withCL_a.vec[i], &noCL_a.vec[i], sizeof(uint32_t) * N);
            memcpy(&withCL_a.vec[i], &noCL_a.vec[i], sizeof(uint32_t) * N);
            for(int k=0; k<N;k++){
            	withCL_a.vec[i].coeffs[k+N] = noCL_b.vec[i].coeffs[k];
            	if(DEBUG)
            	printf("%d: %d*%d = %d\n",k,noCL_a.vec[i].coeffs[k],noCL_b.vec[i].coeffs[k],noCL_a.vec[i].coeffs[k]*noCL_b.vec[i].coeffs[k]);
			}
        }

        //1) NTT for c-code
        timespec_get(&start_cpu, TIME_UTC);
        for (int j = 0; j < Iters; ++j) {
            for (int i = 0; i < polyNum; ++i) {
            	poly_ntt(&noCL_a.vec[i]);
            	poly_ntt(&noCL_b.vec[i]);
            	poly_pointwise_montgomery(&noCL_c.vec[i],&noCL_a.vec[i],&noCL_b.vec[i]);
            	if(DEBUG){ //print NTT(a)//NTT(b)
            		printf("\n---------------------------------------\nC-NTT pointwise multiplyer ins/outs\n");
					for(int k=0;k<N;k++)
						printf("%d: %d = %d * %d\n",k,noCL_c.vec[i].coeffs[k],noCL_a.vec[i].coeffs[k],noCL_b.vec[i].coeffs[k]);
            	}
            	poly_invntt_tomont(&noCL_c.vec[i]);
            }
        }
        timespec_get(&end_cpu, TIME_UTC);

        timeCPU += ((end_cpu.tv_sec-start_cpu.tv_sec)*1000000000L) + (end_cpu.tv_nsec - start_cpu.tv_nsec);

        //2) NTT for HW-code
        time = 0;
        timespec_get(&start, TIME_UTC);
        for (int j = 0; j < Iters; ++j) {
        	cl_event prof_event; cl_ulong cl_tstart, cl_tend;
        	size_t global_size = 2*N * polyNum;						//double sized buffer for in/out
//        	cl_mem ina_buff; cl_mem inb_buff; cl_mem out_buff;		//buffer variables
        	cl_mem in_out_buff;										//in/out buffer pointer
			//Create Buffers
        	in_out_buff = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint) * global_size, &withCL_a, &err);

			if(DEBUG)printf("Setting kernel arguments.\n");
			cl_uint tmp_rounds = (unsigned int)(log2(float(N)));
			if(DEBUG)printf("NTT Length=%d,rounds=%d\n",N,tmp_rounds);
			err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &in_out_buff);

			if(DEBUG && err <0)perror("Setting kernel arguments failed.\n");
			err = clEnqueueTask(queue,kernel,0,NULL,&prof_event);
			if(DEBUG && err<0)perror("Error in kernel execution");
			//Launch Kernel
			if(DEBUG)printf("Launching kernel...\n");
			err = clEnqueueReadBuffer(queue, in_out_buff, CL_TRUE, 0, sizeof(cl_uint) * global_size, &withCL_a, 0, NULL, NULL);

			if(DEBUG && err<0)perror("Error in output buffer read");
			err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_START, sizeof(cl_tstart), &cl_tstart, NULL);
			err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_END, sizeof(cl_tend), &cl_tend, NULL);
			//Release mem objects
			clReleaseMemObject(in_out_buff);
			time = time + cl_tend - cl_tstart;
        }
        timespec_get(&end, TIME_UTC);
        timeOCL += ((end.tv_sec-start.tv_sec)*1000000000L) + (end.tv_nsec - start.tv_nsec);
        timeOCL_kernel += time;

        //---------------------------------------------------------------------------------
        //Compare results with reference
        for (int i = 0; i < polyNum; ++i) {
//        	printf("\n--------------------\nComparing with multiplication output)\n");
        	for( int k =0 ; k<N ; k++){
        		if(noCL_c.vec[i].coeffs[k] != withCL_a.vec[i].coeffs[k]){
        			tv_missmatch = 1;
        			printf("%d: %d ~= %d\n",k,noCL_c.vec[i].coeffs[k],withCL_a.vec[i].coeffs[k]); //comp with _c
//        			break;
        		}
        	}
        }
    }
    if(tv_missmatch)printf("----------------------\nVectors missmatch between CL and No-CL run.\n----------------------\n");
    else printf("----------------------\nTEST PASSED.\n----------------------\n");
    printf("----------------------\nTotal Times\n----------------------\n");
    printf("\n%d CPU NTT operations took %ld nanonseconds\n----------------------\n", NTESTS * Iters, timeCPU);
    printf("\n%d OpenCL NTT_N operation took %ld nanonseconds\n----------------------\n", NTESTS * Iters, timeOCL);
    printf("\n%d OpenCL-kernel NTT_N operation took %ld nanonseconds\n----------------------\n", NTESTS * Iters, timeOCL_kernel);
    printf("----------------------\nAverage Times\n----------------------\n");
    printf("\nAverage CPU NTT operations took %ld nanonseconds\n----------------------\n", timeCPU/(NTESTS * Iters));
    printf("\nAverage OpenCL NTT_N operation took %ld nanonseconds\n----------------------\n", timeOCL/(NTESTS * Iters));
    printf("\nAverage OpenCL-kernel NTT_N operation took %ld nanonseconds\n----------------------\n", timeOCL_kernel/(NTESTS * Iters));
    //---------------------------------------------------------------------------------
    //Release CL objects
//    printf("Releasing OpenCL objects.\n");
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
	clReleaseKernel(kernel);
    return 0;
}

int load_file_to_memory(const char *filename, char **result) {
    uint size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL) {
        *result = NULL;
        return -1; // -1 means file opening fail
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *)malloc(size+1);
    if (size != fread(*result, sizeof(char), size, f)) {
        free(*result);
        return -2; // -2 means file reading fail
    }
    fclose(f);
    (*result)[size] = 0;
    return size;
}
