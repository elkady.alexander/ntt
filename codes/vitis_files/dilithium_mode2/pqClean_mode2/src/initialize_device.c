//#include <stdint.h>
//#include <stdlib.h>
//#include <stdio.h>
//#include <cstring>
//#include "xcl2.hpp"
////#include "CL/cl.h"
//
//int load_file_to_memory(const char *filename, char **result) {
//    uint size = 0;
//    FILE *f = fopen(filename, "rb");
//    if (f == NULL) {
//        *result = NULL;
//        return -1; // -1 means file opening fail
//    }
//    fseek(f, 0, SEEK_END);
//    size = ftell(f);
//    fseek(f, 0, SEEK_SET);
//    *result = (char *)malloc(size+1);
//    if (size != fread(*result, sizeof(char), size, f)) {
//        free(*result);
//        return -2; // -2 means file reading fail
//    }
//    fclose(f);
//    (*result)[size] = 0;
//    return size;
//}
//
//void initialize_device(const char* kernel_file, const char* kernel_name, cl_context* context, cl_kernel* kernel, cl_command_queue* queue ){
//    //Basic variables
//    cl_program program; cl_device_id devices[16]; cl_device_id device_id = 0; cl_int err;
//    //Platform initialization
//	cl_platform_id platform_id = 0;	cl_uint num_platforms;
//    //Get platforms
//	err = clGetPlatformIDs(0, NULL, &num_platforms);
//	if (err < 0) perror("Couldn't find any platforms.");
//	cl_platform_id *platform_ids = (cl_platform_id*)malloc(sizeof(cl_platform_id) * num_platforms);
//	clGetPlatformIDs(num_platforms, platform_ids, NULL);
//	//Select platform Xilinx
//	char cl_platform_vendor[1001];
//	for (int i = 0; i < int(num_platforms); i++) {
//		err = (cl_int) clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, 1000,
//												(void *)cl_platform_vendor, NULL);
//		if(strcmp(cl_platform_vendor,"Xilinx") == 0){
//			printf("Selected platform %d vendor: %s\n---------------------------------\n", i, cl_platform_vendor);
//			platform_id = platform_ids[i];
//		}
//	}
//	//Select device id
//	cl_uint num_devices;
//	err = (cl_int) clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &num_devices);
//	char cl_device_name[1001];
//	for(int i=0; i< int(num_devices); i++){
//		clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name, 0);
//		printf("Device %d : %s\n",i,cl_device_name);
//		device_id = devices[i];
//	}
//	//Create Context
//	*context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
//	//Create Command Queue
//	*queue = clCreateCommandQueue(*context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
//	//Create program kernel
//	cl_int err_code; unsigned char *krnl_bin;
////	program = clCreateProgramWithSource(context, 1, (const char**)&program_buffer, &program_size, &err);
//    const size_t krnl_size = load_file_to_memory(kernel_file, (char **) &krnl_bin);
//    program = clCreateProgramWithBinary(*context, 1, &device_id, &krnl_size,
//                                        (const unsigned char **) &krnl_bin, &err, &err_code);
//    if(err<0)perror("Error in kernel creation");
//	//Create kernel objects
//	*kernel = clCreateKernel(program, kernel_name , &err);
//	if(err <0) perror("Error initializing kernel");
//	size_t wg_size;
//	err = clGetKernelWorkGroupInfo(*kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE,	sizeof(wg_size), &wg_size, NULL);
//	printf("Kernel workgroup size = %d\n",int(wg_size));
//	//release memory from objects
//	clReleaseDevice(device_id);
//	clReleaseProgram(program);
//	free(platform_ids);
//}
