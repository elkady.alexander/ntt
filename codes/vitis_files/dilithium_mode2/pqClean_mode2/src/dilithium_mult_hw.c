//#include <stdint.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include "params.h"
//#include "CL/cl.h"
//#include "poly.h"
//#include "polyvec.h"
//#include <math.h>
//
//#define DEBUG 1
//
//const char *kernel_name = "dilithium2_mm";
//const char *kernel_file = "hsm_postquantum.xclbin";
//
//void initialize_device(const char* kernel_file, const char* kernel_name, cl_context* context, cl_kernel* kernel, cl_command_queue* queue );
//
////Basic variables
//cl_context context;
//cl_kernel kernel; cl_command_queue queue; cl_int err;
//
////timing variables
//cl_event prof_event; cl_ulong cl_tstart, cl_tend;
//cl_ulong exec_time;
//
////Input *a , contains both in1,in2 inputs , a = [in1,in2]
////Output is saved in *a, which is *a = in1*in2
//long dilithium_mult(polyvecl *polyvec_a,polyvecl mat[K], polyveck *polyvec_out){
//	exec_time = 0;
//	int N = N_const;
//	cl_event prof_event; cl_ulong cl_tstart, cl_tend;
//	size_t global_size = N*K;						//double sized buffer for in/out
////        	cl_mem ina_buff; cl_mem inb_buff; cl_mem out_buff;		//buffer variables
//	cl_mem in_out_buff;										//in/out buffer pointer
//
//	//Platform initialization
//	initialize_device(kernel_file,kernel_name,&context,&kernel,&queue);
//
//	printf("Device initialized\n");
//	//Platform initialization
//	//Create Buffers
//	in_out_buff = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint) * global_size, polyvec_a, &err);
//
//	if(DEBUG)printf("Setting kernel arguments.\n");
//	cl_uint tmp_rounds = (unsigned int)(log2(float(N)));
//	if(DEBUG)printf("NTT Length=%d,rounds=%d\n",N,tmp_rounds);
//	err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &in_out_buff);
//	err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &in_out_buff);
//	if(DEBUG && err <0)perror("Setting kernel arguments failed.\n");
//	err = clEnqueueTask(queue,kernel,0,NULL,&prof_event);
//	if(DEBUG && err<0)perror("Error in kernel execution");
//	//Launch Kernel
////	if(DEBUG)for(int i=0;i<N;i++)
////		printf("%d: %d ~= %ld\n",i,polyvec_a->vec[0].coeffs[i],polyvec_out->vec[0].coeffs[i]);
//	if(DEBUG)printf("Size of buffer = %ld",sizeof(unsigned int) * N * K * 4);
//	//input vector
//	unsigned int source[N*L];
//	int tmp = 0;
//	for(int k=0;k<K;k++)
//		for(int i=0;i<N;i++){
//			source[tmp]=polyvec_a->vec[k].coeffs[i];
////			if(k==L)
////			printf("%d,%d: %d\\%d\n",k,i,source[tmp],polyvec_a->vec[k].coeffs[i]);
//			tmp++;
////			//check mat[K] array
////			if(DEBUG && k==K){
////				int l=0;
////				printf("%d: %d, ",i,mat[k].vec[l].coeffs[i]);
////			}
//		}
//	if(DEBUG)printf("Launching kernel...\n");
//	// Assuming K == L
//	err = clEnqueueReadBuffer(queue, in_out_buff, CL_TRUE, 0, sizeof(unsigned int) * N * K * 4, source, 0, NULL, NULL);
//	if(DEBUG)printf("Completed kernel launch...\n");
//	//copy to output vector
//	tmp = 0;
//	for(int i=0;i<K;i++)
//		for(int j=0;j<N;j++)
//			polyvec_out->vec[i].coeffs[j] = source[tmp++];
//
//	if(DEBUG && err<0)perror("Error in output buffer read");
//	err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_START, sizeof(cl_tstart), &cl_tstart, NULL);
//	err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_END, sizeof(cl_tend), &cl_tend, NULL);
//	//Release mem objects
//	clReleaseMemObject(in_out_buff);
//	exec_time = exec_time + cl_tend - cl_tstart;
//	return (long (exec_time));
//}
