//#include <stdint.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include "CL/cl.h"
//#include "poly.h"
//#include "polyvec.h"
//#include <math.h>
//#include "xcl2.hpp"
////#include "params.h"		//already included
//#include <algorithm>
//#include <vector>
//
//#define DEBUG 0
//#define N 256
//
//const char *kernel_name = "dilithium2_mm";
//const char *binary_file = "hsm_postquantum.xclbin";
//
////void initialize_device(const char* kernel_file, const char* kernel_name, cl_context* context, cl_kernel* kernel, cl_command_queue* queue );
//void initialize_device_ocl(std::string binary_file, const char* kernel_name, cl::Context* context, cl::Kernel* kernel, cl::CommandQueue* queue, const int debug);
//
//cl_int err;
//
////timing variables
//cl::Event prof_event;
//cl_ulong exec_time = 0;
//
//unsigned long calc_full_exec_time(cl::Event &wait_event) {
//    unsigned long queue, submit, start, stop;
//    cl_int err;
//
//    OCL_CHECK(err,
//              err = wait_event.getProfilingInfo<unsigned long>(
//            		  CL_PROFILING_COMMAND_QUEUED, &queue));
//    OCL_CHECK(err,
//              err = wait_event.getProfilingInfo<unsigned long>(
//            		  CL_PROFILING_COMMAND_SUBMIT, &submit));
//    OCL_CHECK(err,
//              err = wait_event.getProfilingInfo<unsigned long>(
//                  CL_PROFILING_COMMAND_START, &start));
//    OCL_CHECK(err,
//              err = wait_event.getProfilingInfo<unsigned long>(
//                  CL_PROFILING_COMMAND_END, &stop));
//    unsigned long queueing =  (submit - queue);
//    unsigned long submitting =  (start - submit);
//    unsigned long executing =  (stop - start);
//    unsigned long full_time = (stop - queue);
//
//    if(DEBUG)printf("PROFILING(ns): queue|submit|exec|full: %lu|%lu|%lu|%lu\n",queueing,submitting,executing,full_time);
//    return full_time;
//
//}
//
////Input *a , contains both in1,in2 inputs , a = [in1,in2]
////Output is saved in *a, which is *a = in1*in2
//unsigned long dilithium_mult_ocl(polyvecl *polyvec_a,polyvecl mat[K], polyveck *polyvec_out
////		,long int *io_time,long int *buf_io_time,long int *others_time, long int *out_io_time
//		){
//	static int device_initialized = -1;
//	//Basic variables
//	static cl::Context context;
//	static cl::Kernel kernel;
//	static cl::CommandQueue queue;
//	static cl::Buffer buffer_in1, buffer_in2, buffer_output;
//
//	exec_time = 0;
//	size_t data_size_K = N*K* sizeof(int);											//polyeveck of N integers
//	size_t data_size_L = N*L * sizeof(int);											//polyevecl of N integers
//	size_t data_size_KL = N*K*L * sizeof(int);							//polyvecl[K] of N integers
//	//Platform initialization
//	static std::vector<int, aligned_allocator<int> > source_in1(N*L);
//	static std::vector<int, aligned_allocator<int> > source_in2(N*L*K);
//	static std::vector<int, aligned_allocator<int> > source_hw_results(N*K);
//	if(device_initialized == -1){
//		initialize_device_ocl(binary_file,kernel_name,&context,&kernel,&queue,DEBUG);
//		device_initialized = 1;
//		if(DEBUG)printf("Device initialized\n");
//		//polyvecl
//	    OCL_CHECK(err, buffer_in1 = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, data_size_L,
//	                                         source_in1.data(), &err));
//	    //polyvecl[K]
//	    OCL_CHECK(err, buffer_in2 = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, data_size_KL,
//	                                         source_in2.data(), &err));
//	    //polyveck
//	    OCL_CHECK(err, buffer_output = cl::Buffer(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, data_size_K,
//	                                            source_hw_results.data(), &err));
//
//	    if(DEBUG)printf("Setting kernel arguments.\n");
//	    OCL_CHECK(err, err = kernel.setArg(0, buffer_in1));
//	    OCL_CHECK(err, err = kernel.setArg(1, buffer_in2));
//	    OCL_CHECK(err, err = kernel.setArg(2, buffer_output));
//	}
//	// Allocate Memory in Host Memory
//	// When creating a buffer with user pointer (CL_MEM_USE_HOST_PTR), under the
//	// hood user ptr
//	// is used if it is properly aligned. when not aligned, runtime had no choice
//	// but to create
//	// its own host side buffer. So it is recommended to use this allocator if
//	// user wish to
//	// create buffer using CL_MEM_USE_HOST_PTR to align user buffer to page
//	// boundary. It will
//	// ensure that user buffer is used when user create Buffer/Mem object with
//	// CL_MEM_USE_HOST_PTR
//
//    // Allocate Buffer in Global Memory
//    // Buffers are allocated using CL_MEM_USE_HOST_PTR for efficient memory and
//    // Device-to-host communication
//	//input vector in1
//	int tmp1 = 0;
//	for (int k=0;k<L;k++){
//		for(int i=0;i<N;i++){
//			source_in1[tmp1++]=polyvec_a->vec[k].coeffs[i];
////			source_in1[tmp1++] = 0;
////			printf("%d,%d: %d\\%d\n",k,i,source_in1[tmp1-1],polyvec_a->vec[k].coeffs[i]);
//		}
//	}
//
//	//input vector in2
//	int tmp2 = 0;
//	for(int k=0;k<K;k++)
//		for (int l=0;l<L;l++)
//			for(int i=0;i<N;i++){
//				source_in2[tmp2++]=mat[k].vec[l].coeffs[i];
////				if(DEBUG && k==K-1){
////					printf("mat[4,%d]: %d\\%d, ",i,source_in2[tmp2-1],mat[k].vec[l].coeffs[i]);
////				}
//			}
//	//initialize output buffer to 0
////	for(int i=0;i<N*K;i++)
////		source_hw_results[i] = 0;
//
//
//    // Copy input data to device global memory
//	OCL_CHECK(err, err = queue.enqueueMigrateMemObjects({buffer_in1, buffer_in2}, 0 /* 0 means from host*/));
////	if(DEBUG && err <0)perror("Setting kernel arguments failed.\n");
////	cl_uint tmp_rounds = (unsigned int)(log2(float(N)));
////	if(DEBUG)printf("NTT Length=%d,rounds=%d\n",N,tmp_rounds);
//    // Launch the Kernel
//    // For HLS kernels global and local size is always (1,1,1). So, it is
//    // recommended
//    // to always use enqueueTask() for invoking HLS kernel
////	if(DEBUG)printf("Launching kernel...\n");
////    OCL_CHECK(err, err = queue.enqueueTask(kernel,NULL,&prof_event));
//    OCL_CHECK(err, err = queue.enqueueTask(kernel,NULL));
////	if(DEBUG && err<0)perror("Error in kernel execution");
////	if(DEBUG)printf("Completed kernel launch...\n");
//    // Copy Result from Device Global Memory to Host Local Memory
//	OCL_CHECK(err, err = queue.enqueueMigrateMemObjects({buffer_output}, CL_MIGRATE_MEM_OBJECT_HOST));
////	if(DEBUG && err<0)perror("Error in output buffer read");
////	prof_event.wait();
//	queue.finish();
//	// OPENCL HOST CODE AREA END
//	//copy to output vector
//	int tmp = 0;
//	for(int i=0;i<K;i++)
//		for(int j=0;j<N;j++)
//			polyvec_out->vec[i].coeffs[j] = source_hw_results[tmp++];
//	//calculate execution time
//    //	printf("Getting profile info\n");
////	cl_tstart = prof_event.getProfilingInfo(CL_PROFILING_COMMAND_START, &queue);
////	printf("ping");
////	cl_tend = prof_event.getProfilingInfo(CL_PROFILING_COMMAND_END, &queue);
////	if(DEBUG)printf("Completed profiling");
//	return 0;//
////	return calc_full_exec_time(prof_event);
//}
