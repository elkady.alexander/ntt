#include "CL/cl.h"
#include "xcl2.hpp"
#include <vector>

unsigned long dilithium_mult_ocl(polyvecl *polyvec_a,polyvecl mat[K], polyveck *polyvec_out,
		long int *in_io_time,long int *buf_io_time,long int *migrate_in_time,
		long int *migrate_out_time,long int *out_io_time);

//Platform initialization
void initialize_device(const char* kernel_file, const char* kernel_name, \
		cl_context* context, cl_kernel* kernel, cl_command_queue* queue,const int DEBUG);
