//#include <stdint.h>
//#include <stdlib.h>
//#include <stdio.h>
//#include <cstring>
//#include "xcl2.hpp"
////#include "CL/cl.h"
//#define N 256
//#include "params.h"
//
//size_t data_size_K = N*K* sizeof(int);		//polyeveck of N integers
//size_t data_size_L = N*L * sizeof(int);		//polyevecl of N integers
//size_t data_size_KL = N*K*L * sizeof(int);	//polyvecl[K] of N integers
//
//void initialize_device_ocl2(std::string binary_file, const char* kernel_name, cl::Context* context, cl::Kernel* kernel, cl::CommandQueue* queue,const int DEBUG,
//		cl::Buffer* buffer_in1_out,cl::Buffer* buffer_in2_out,cl::Buffer* buffer_output_out,
//		void* source_in1,
//		void* source_in2,
//		void* source_hw_results){
//    // OPENCL HOST CODE AREA START
//    // get_xil_devices() is a utility API which will find the xilinx
//    // platforms and will return list of devices connected to Xilinx platform
//    auto devices = xcl::get_xil_devices();
//    // read_binary_file() is a utility API which will load the binaryFile
//    // and will return the pointer to file buffer.
//    auto fileBuf = xcl::read_binary_file(binary_file);
//    cl::Program::Binaries bins{{fileBuf.data(), fileBuf.size()}};
//    bool valid_device = false;
//    cl_int err;
//    for (unsigned int i = 0; i < devices.size(); i++) {
//        auto device = devices[i];
//        // Creating Context and Command Queue for selected Device
//        OCL_CHECK(err, *context = cl::Context(device, nullptr, nullptr, nullptr, &err));
////        *context = cl::Context(device, nullptr, nullptr, nullptr, &err);
//        OCL_CHECK(err, *queue = cl::CommandQueue(*context, device, CL_QUEUE_PROFILING_ENABLE, &err));
////        *queue = cl::CommandQueue(*context, device, CL_QUEUE_PROFILING_ENABLE, &err);
//        if(DEBUG)std::cout << "Trying to program device[" << i << "]: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
//        cl::Program program(*context, {device}, bins, nullptr, &err);
//        if (err != CL_SUCCESS) {
//        	if(DEBUG)std::cout << "Failed to program device[" << i << "] with xclbin file!\n";
//        } else {
//        	if(DEBUG)std::cout << "Device[" << i << "]: program successful!\n";
//            OCL_CHECK(err, *kernel = cl::Kernel(program, kernel_name, &err));
//            valid_device = true;
//            break; // we break because we found a valid device
//        }
//    }
//    if (!valid_device) {
//    	if(DEBUG)std::cout << "Failed to program any device found, exit!\n";
//        exit(EXIT_FAILURE);
//    }
//    static cl::Buffer buffer_in1,buffer_in2,buffer_output;
//    //polyvecl
//	OCL_CHECK(err, buffer_in1(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, data_size_L,
//										source_in1, &err));
//	//polyvecl[K]
//	OCL_CHECK(err, buffer_in2(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, data_size_KL,
//										 source_in2, &err));
//	//polyveck
//	OCL_CHECK(err, buffer_output(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, data_size_K,
//											source_hw_results, &err));
//	buffer_in1_out = &buffer_in1;
//	buffer_in2_out = &buffer_in2;
//	buffer_output_out = &buffer_output;
//
//	if(DEBUG)printf("Setting kernel arguments.\n");
//	OCL_CHECK(err, err = kernel->setArg(0, buffer_in1));
//	OCL_CHECK(err, err = kernel->setArg(1, buffer_in2));
//	OCL_CHECK(err, err = kernel->setArg(2, buffer_output));
//}
