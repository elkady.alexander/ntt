//NTT and INTT HLS-code for Dilithium
//This code is running for max N=2^16
#define QINV 58728449 // q^(-1) mod 2^32
#define Q 8380417
#define N_const 256
//#define K 8
//#define L 7
#define Stages 8 //!!log2(N)!!
#define B 2 //need to check for higher B how memory-map works

#include "params.h"
#include <hls_vector.h>
#include <stdio.h>
#include "ap_int.h"
//typedef ap_uint<23> uint23_t;
//typedef ap_uint<46> uint46_t;

// Primitive roots of unity
static const int32_t zetas[N_const] = {
    0, 25847, -2608894,  -518909,   237124,  -777960,  -876248,   466468,
    1826347,  2353451,  -359251, -2091905,  3119733, -2884855,  3111497,  2680103,
    2725464,  1024112, -1079900,  3585928,  -549488, -1119584,  2619752, -2108549,
    -2118186, -3859737, -1399561, -3277672,  1757237,   -19422,  4010497,   280005,
    2706023,    95776,  3077325,  3530437, -1661693, -3592148, -2537516,  3915439,
    -3861115, -3043716,  3574422, -2867647,  3539968,  -300467,  2348700,  -539299,
    -1699267, -1643818,  3505694, -3821735,  3507263, -2140649, -1600420,  3699596,
    811944,   531354,   954230,  3881043,  3900724, -2556880,  2071892, -2797779,
    -3930395, -1528703, -3677745, -3041255, -1452451,  3475950,  2176455, -1585221,
    -1257611,  1939314, -4083598, -1000202, -3190144, -3157330, -3632928,   126922,
    3412210,  -983419,  2147896,  2715295, -2967645, -3693493,  -411027, -2477047,
    -671102, -1228525,   -22981, -1308169,  -381987,  1349076,  1852771, -1430430,
    -3343383,   264944,   508951,  3097992,    44288, -1100098,   904516,  3958618,
    -3724342,    -8578,  1653064, -3249728,  2389356,  -210977,   759969, -1316856,
    189548, -3553272,  3159746, -1851402, -2409325,  -177440,  1315589,  1341330,
    1285669, -1584928,  -812732, -1439742, -3019102, -3881060, -3628969,  3839961,
    2091667,  3407706,  2316500,  3817976, -3342478,  2244091, -2446433, -3562462,
    266997,  2434439, -1235728,  3513181, -3520352, -3759364, -1197226, -3193378,
    900702,  1859098,   909542,   819034,   495491, -1613174,   -43260,  -522500,
    -655327, -3122442,  2031748,  3207046, -3556995,  -525098,  -768622, -3595838,
    342297,   286988, -2437823,  4108315,  3437287, -3342277,  1735879,   203044,
    2842341,  2691481, -2590150,  1265009,  4055324,  1247620,  2486353,  1595974,
    -3767016,  1250494,  2635921, -3548272, -2994039,  1869119,  1903435, -1050970,
    -1333058,  1237275, -3318210, -1430225,  -451100	,  1312455,  3306115, -1962642,
    -1279661,  1917081, -2546312, -1374803,  1500165,   777191,  2235880,  3406031,
    -542412, -2831860, -1671176, -1846953, -2584293, -3724270,   594136, -3776993,
    -2013608,  2432395,  2454455,  -164721,  1957272,  3369112,   185531, -1207385,
    -3183426,   162844,  1616392,  3014001,   810149,  1652634, -3694233, -1799107,
    -3038916,  3523897,  3866901,   269760,  2213111,  -975884,  1717735,   472078,
    -426683,  1723600, -1803090,  1910376, -1667432, -1104333,  -260646, -3833893,
    -2939036, -2235985,  -420899, -2286327,   183443,  -976891,  1612842, -3545687,
    -554416,  3919660,   -48306, -1362209,  3937738,  1400424,  -846154,  1976782
};

int32_t montgomery_reduce(int64_t a) {
    int32_t t;

    t = (int32_t)((uint64_t)a * (uint64_t)QINV);
    t = (a - (int64_t)t * Q) >> 32;
    return t;
}

int32_t montgomery_reduce32(int32_t a) {
    int32_t t;

    t = (a + (1 << 22)) >> 23;
    t = a - t * Q;
    return t;
}

// Parity function for 16-bit input
bool findParity(ap_uint<16> x)
{
    // recursively divide the (32–bit) integer into two equal
    // halves and take their XOR until only 1 bit is left
    x = (x & 0x000000FF) ^ (x >> 8);
    x = (x & 0x0000000F) ^ (x >> 4);
    x = (x & 0x00000003) ^ (x >> 2);
    x = (x & 0x00000001) ^ (x >> 1);

    return x & 1;
}

// Rotate left function for integer values
unsigned int rotateLeft(unsigned int x,unsigned int d)
{
	//int_bits = Stages
	//In n<<d, last d bits are 0.
	//To put first Stages-d bits of n at last, do bitwise or of n<<d
	//with n >>(INT_BITS - d)
	return ( ((x << d) & ((1<<Stages)-1)) | (x >> (Stages - d)) );
}


//NTT_256_hls TEST PASSED in SW-Emulation and Hardware
void NTT_256_hls(int32_t* p_out,int32_t* p)
{
#pragma HLS DATAFLOW
	int32_t localRegs[N_const/2][2] ;
//#pragma HLS array_partition variable=localRegs complete
#pragma HLS array_partition variable=localRegs block factor=2 dim=2

  readInput: for(int i=0;i<N_const;i++){
#pragma HLS pipeline
	  int idx = i>>1;
	  if(findParity(i) ==1){
		  localRegs[idx][1] = p[i];
	  }
	  else{
		  localRegs[idx][0] = p[i];
	  }
  }

  unsigned int u = N_const/2;
  //NTT: log2(N) rounds, each round has N/2 PEs in total
  STAGE_LOOP: for(int i= 0; i < Stages; i++) {
 		//On each stage: for each butterfy-loop create B PEs
	  BUTTERFLY_LOOP: for(int s = 0; s < u; s = s + B) {
#pragma HLS pipeline
//no data dependencies between different loops
#pragma HLS dependence variable=localRegs inter false
		  //feedback regs for each stage
		  ap_uint<16> j[B],k[B],i_w[B]; //idx arrays
		  ap_uint<16> i_e[B],i_o[B];
		  int32_t U[B],V[B],W[B],E[B],O[B];
		  //  uint23_t U[B],V[B],W[B],E[B],O[B]; (not working)
		  bool parity[B]; //calc arrays
#pragma HLS array_partition variable=i_e complete
#pragma HLS array_partition variable=i_o complete
#pragma HLS array_partition variable=i_w complete
#pragma HLS array_partition variable=U complete
#pragma HLS array_partition variable=V complete
#pragma HLS array_partition variable=W complete
#pragma HLS array_partition variable=E complete
#pragma HLS array_partition variable=O complete

		  IDX_CALC_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
			  unsigned int tmp = s+b;
			  i_e[b] = rotateLeft((tmp<<1),Stages-1-i); //low index
			  i_o[b] = rotateLeft((tmp<<1) + 1 ,Stages-1-i); //high index
			  parity[b] = findParity(i_e[b]);
			  i_w[b] = (1<<i) + (tmp % (1<<i));/// (1<<(l-i-1)));
		  }

		  MEM_READ_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
			  int idx_e = i_e[b]>>1;
			  int idx_o = i_o[b]>>1;
			  if(parity[b] == 1){
				  U[b] = localRegs[idx_e][1];
				  V[b] = localRegs[idx_o][0];
			  }
			  else{
				  U[b] = localRegs[idx_e][0];
				  V[b] = localRegs[idx_o][1];
			  }
			  W[b] = zetas[i_w[b]];
		  }

		  OP_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
//			  if(i==0){
//				printf("%d: a[j+len]=%d, a[j]=%d, zeta=%d\t",s,V[b],U[b],W[b]);
//			  }
			  int32_t t = montgomery_reduce((int64_t)W[b] * V[b]);
			  O[b] = U[b] - t;
			  E[b] = U[b] + t;
//			  if(i==0){
//				printf("//a[j+len]=%d, a[j]=%d, t=%d\n",O[b],E[b],t);
//			  }
		  }

		  MEM_WRITE_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll
			  int idx_e = i_e[b]>>1;
			  int idx_o = i_o[b]>>1;
			  if(parity[b] == 1){
				  localRegs[idx_e][1] = E[b];
				  localRegs[idx_o][0] = O[b];
			  }
			  else{
				  localRegs[idx_e][0] = E[b];
				  localRegs[idx_o][1] = O[b];
			  }
		  }
	  }
  }

  //Write final coefficients to output RAM
  writeOutput: for(int i=0;i<N_const;i++){
#pragma HLS pipeline
	  int idx = i>>1;
	  if(findParity(i) == 0){
		  p_out[i] = localRegs[idx][0];
	  }
	  else{
		  p_out[i] = localRegs[idx][1];
	  }
  }
}

//INTT_256_hls TEST PASSED in SW-Emulation and Hardware
void INTT_256_hls(int32_t* p_out,int32_t* p)
{
#pragma HLS DATAFLOW
	int32_t localRegs[N_const/2][2];
//#pragma HLS array_partition variable=localRegs complete
#pragma HLS array_partition variable=localRegs block factor=2 dim=2
  //Read input and write on RAM based on memory-map scheme
  readInput: for(int i=0;i<N_const/2;i++){
#pragma HLS pipeline
	  ap_uint<16> e_tmp = rotateLeft(i<<1,0);
	  ap_uint<16> o_tmp = rotateLeft((i<<1)+1,0);
	  int idx_e = e_tmp>>1;
	  int idx_o = o_tmp>>1;
	  if(findParity(e_tmp) == 1){
		  localRegs[idx_e][1] = p[e_tmp];
		  localRegs[idx_o][0] = p[o_tmp];
	  }
	  else{
		  localRegs[idx_e][0] = p[e_tmp];
		  localRegs[idx_o][1] = p[o_tmp];
	  }
  }

  //INTT: log2(N) rounds, each round has N/2 PEs in total
  unsigned int u = N_const/2; int32_t z_index = 0;
  STAGE_LOOP: for(int i= Stages-1; i >= 0; i--) {
//	  printf("%Stage %d\n",i);
  	//On each stage: for each butterfly-loop create B PEs
	  BUTTERFLY_LOOP: for(int s = 0; s < u; s = s + B) {
#pragma HLS pipeline
	//no data dependencies between different loops
#pragma HLS dependence variable=localRegs inter false
		  //feedback regs for each stage
		  ap_uint<16> i_w[B]; //idx arrays
		  ap_uint<16> i_e[B],i_o[B];
		  int32_t U[B],V[B],W[B],E[B],O[B]; //calc arrays
		  bool parity[B];
#pragma HLS array_partition variable=i_e complete
#pragma HLS array_partition variable=i_o complete
#pragma HLS array_partition variable=i_w complete
#pragma HLS array_partition variable=U complete
#pragma HLS array_partition variable=V complete
#pragma HLS array_partition variable=W complete
#pragma HLS array_partition variable=E complete
#pragma HLS array_partition variable=O complete

		  IDX_CALC_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  unsigned int tmp = s+b;
			  i_e[b] = rotateLeft((tmp<<1),Stages-1-i); //low index
			  i_o[b] = rotateLeft((tmp<<1) + 1 ,Stages-1-i); //high index
			  parity[b] = findParity(i_e[b]);
			  i_w[b] = z_index + (tmp % (1<<i));

		  }

		  MEM_READ_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  int idx_e = i_e[b]>>1;
			  int idx_o = i_o[b]>>1;
			  if(parity[b] == 1){
				  U[b] = localRegs[idx_e][1];
				  V[b] = localRegs[idx_o][0];
			  }
			  else{
				  U[b] = localRegs[idx_e][0];
				  V[b] = localRegs[idx_o][1];
			  }
			  W[b] = -zetas[N_const-1-i_w[b]];
		  }

		  OP_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  E[b] = U[b] + V[b];
			  int32_t tmp_reg = U[b] - V[b];
			  O[b] = montgomery_reduce((int64_t)W[b] * tmp_reg);
		  }

		  MEM_WRITE_LOOP: for(int b = 0; b < B; b++){
			#pragma HLS unroll factor = 2
			  int idx_e = i_e[b]>>1;
			  int idx_o = i_o[b]>>1;
			  if(parity[b] == 0){
				  localRegs[idx_e][0] = E[b];
				  localRegs[idx_o][1] = O[b];
			  }
			  else{
				  localRegs[idx_e][1] = E[b];
				  localRegs[idx_o][0] = O[b];
			  }
		  }
	  }
	  //Update z_index on each stage
	  z_index = z_index + (1<<i);
  }

  const int32_t f = 41978; // mont^2/256
  mont_reduce: for(int i=0;i<N_const/2;i++){
#pragma HLS pipeline
	  localRegs[i][0] = montgomery_reduce((int64_t)f*localRegs[i][0]);
	  localRegs[i][1] = montgomery_reduce((int64_t)f*localRegs[i][1]);
  }

  //Write final coefficients to output RAM
  writeOutput: for(int i=0;i<N_const;i++){
#pragma HLS pipeline
	  int idx = i>>1;
	  if(findParity(i) == 0){
		  p_out[i] = localRegs[idx][0];
	  }
	  else{
		  p_out[i] = localRegs[idx][1];
	  }
  }
}

void polyvecl_pointwise_acc_montgomery2(int32_t* out,
								const int32_t in1[N_const * L],
								const int32_t in2[N_const * L]){
#pragma HLS DATAFLOW
	int32_t tmp2[N_const]; //variable holding the accumulated sum
	//----initial loop of multiplication for L=0----//
	for (int i=0;i<N_const;i++){
//		#pragma hls unroll factor=2
		tmp2[i] = montgomery_reduce((int64_t)in1[i]*in2[i]);
	}

	//----other loops for L=1,2,3..----//
	for(int i=1;i<L;i++){
		int offset = i*N_const;
		for (int k=0;k<N_const;k++){
//			#pragma hls unroll factor=2
			int idx = offset+k;
			int32_t tmp = montgomery_reduce((int64_t)in1[idx]*in2[idx]);
			tmp2[k] = tmp2[k] + tmp;
		}
	}

	//----montgomery 32-bit and write to output----//
	for(int i=0;i<N_const;i++){
//		#pragma hls unroll factor=2
		out[i] = montgomery_reduce32(tmp2[i]);
	}
}

extern "C"{

void dilithium2_mm(const int32_t *in,const int32_t *mat,int32_t *out){
#pragma hls dataflow
#pragma HLS INTERFACE m_axi port = in bundle = gmem0
#pragma HLS INTERFACE m_axi port = mat bundle = gmem1
#pragma HLS INTERFACE m_axi port = out bundle = gmem0
	typedef int32_t data_type;
	data_type poly_vector[L][N_const];
	// K = 6 , L = 5
#pragma HLS array_partition variable=poly_vector type=block dim=1 factor=7 //factor = L 4,5,7 hardcoded
	//-----------------------Read of polyvecl input-----------------------//
	int tmp1 = 0;
	readInput_vector:
	for(int i=0;i<L;i++){
		for(int j=0;j<N_const;j++){
			#pragma hls unroll factor=2
			poly_vector[i][j] = in[tmp1];
			tmp1 = tmp1 + 1;
		}
	}

	//-----------------------Read of polyvecl mat-----------------------//
	data_type poly_vector_mat[K][L][N_const];
#pragma HLS array_partition variable=poly_vector_mat type=block dim=1 factor=8 //factor = K 4,6,8 hardcoded
	read_strm_mat:
	int tmp2 = 0;
	readInput_mat:
	for(int k=0;k<K;k++){
		for(int i=0;i<L;i++){
			for(int j=0;j<N_const;j++){
				#pragma hls unroll factor=2
				poly_vector_mat[k][i][j] = mat[tmp2];
				tmp2 = tmp2 + 1;
			}
		}
	}

//---------------------------L NTT-calls---------------------------//
	data_type poly_vector_nnt[L][N_const];
//#pragma HLS array_partition variable=poly_vector_nnt type=block dim=1 factor=4
	NTT_executions:
	for (int i=0;i<L;i++){
		NTT_256_hls(poly_vector_nnt[i],poly_vector[i]);
	}

	//--------K Matrix-multiplications of polyvecs a and mat: a X mat--------//
	data_type mult_result[K][N_const];
#pragma HLS array_partition variable=mult_result type=block dim=1 factor=8 //4,6,8
	Matrix_mul:for(int k=0;k<K;k++){
		data_type mult_result_tmp[N_const];

		// multiplication of vector LX256-vec with K different polyvecl LX256 of mat[K] and acc with reduce 32-bit
		polyvecl_pointwise_acc_montgomery2(mult_result_tmp,*poly_vector_nnt,*poly_vector_mat[k]);

//		INTT for each one of K-vectors
		INTT_256_hls(mult_result[k],mult_result_tmp);
	}

	//-------------------Write final polyveck to outputs-------------------//
	int tmp3 = 0;
	Write_2output:
	for (int k=0;k<K;k++){
		#pragma HLS unroll factor = 2
			for (int j=0;j<N_const;j++){
				out[tmp3++] = mult_result[k][j];
			}
	}
}

}
