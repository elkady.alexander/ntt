#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include "../params.h"
#include "../randombytes.h"
#include "../poly.h"
#include "../ntt.h"
#include "../OpenCLHelper.h"
#include "CL/cl.h"
#include <time.h>
#include <math.h>

int load_file_to_memory(const char *filename, char **result);

#define NTESTS 10
//Define number of iterations
#define Iters 1
#define units_kernel 256
#define DEBUG 0

int main(void) {
    struct timespec start, end, start_cpu, end_cpu;
    const char *krnl_file = "NTT_INTT.xclbin";

    //Basic variables
    cl_program program;
    cl_context context;
    cl_device_id devices[16];
    cl_device_id device_id = 0;
    cl_kernel NTTkernel;
    cl_command_queue queue;
    cl_int err;

    //Platform initialization
	cl_platform_id platform_id = 0;
	cl_uint num_platforms;
	cl_int i,j,test;
	//---------------------------------------------------------------------------------
    //Get platforms
	err = clGetPlatformIDs(0, NULL, &num_platforms);
	if (err < 0) {
		perror("Couldn't find any platforms.");
		return err;
	}
	cl_platform_id *platform_ids = (cl_platform_id*)malloc(sizeof(cl_platform_id) * num_platforms);
	clGetPlatformIDs(num_platforms, platform_ids, NULL);
	//---------------------------------------------------------------------------------
	//Select platform Xilinx
	char cl_platform_vendor[1001];
	for (i = 0; i < int(num_platforms); i++) {
		err = (cl_int) clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, 1000,
												(void *)cl_platform_vendor, NULL);
		if(strcmp(cl_platform_vendor,"Xilinx") == 0){
			printf("Selected platform %d vendor: %s\n---------------------------------\n", i, cl_platform_vendor);
			platform_id = platform_ids[i];
		}
	}
	//---------------------------------------------------------------------------------
	//Select device id
	cl_uint num_devices;
	err = (cl_int) clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &num_devices);
	char cl_device_name[1001];
	for(i=0; i< int(num_devices); i++){
		clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name, 0);
		printf("Device %d : %s\n",i,cl_device_name);
		device_id = devices[i];
	}
	//---------------------------------------------------------------------------------
	//Create Context
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	//---------------------------------------------------------------------------------
	//Create Command Queue
	queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
	//---------------------------------------------------------------------------------
	//Create program kernel
	cl_int err_code;
//	program = clCreateProgramWithSource(context, 1, (const char**)&program_buffer, &program_size, &err);
	unsigned char *krnl_bin;
    const size_t krnl_size = load_file_to_memory(krnl_file, (char **) &krnl_bin);
    program = clCreateProgramWithBinary(context, 1, &device_id, &krnl_size,
                                        (const unsigned char **) &krnl_bin,
                                        &err, &err_code);
    if(err<0)perror("Error in kernel creation");
    //---------------------------------------------------------------------------------
	//Create kernel objects
	NTTkernel = clCreateKernel(program, "NTT_256_hls", &err);
	if(err <0) perror("Error initializing kernel NTT_256_hls");
	size_t wg_size;
	err = clGetKernelWorkGroupInfo(NTTkernel, device_id, CL_KERNEL_WORK_GROUP_SIZE,	sizeof(wg_size), &wg_size, NULL);
	printf("Kernel workgroup size = %d\n",int(wg_size));
	/*---------------------------------------------------------------------------------
					Run NTT for 1) C-code and 2) CL-device
	-----------------------------------------------myNTT_256----------------------------------*/
    uint8_t seed[SEEDBYTES];
    uint16_t nonce = 0;
    polyvecNum noCL, withCL;

    cl_ulong time;
    uint32_t diff;
    long int timeCPU =0;
    long int timeOCL =0;
    long int timeOCL_kernel =0;

    printf("\nSize of polynomial: %d \n", polyNum);
    int tv_missmatch = 0;
    randombytes(seed, sizeof(seed));
    for (test = 0; test < NTESTS; ++test) {
        for (i = 0; i < polyNum; ++i) {
            poly_uniform(&noCL.vec[i], seed, nonce++);
            memcpy(&withCL.vec[i], &noCL.vec[i], sizeof(uint32_t) * N);
            if(DEBUG && 0)
            	for(int k=0;k<N;k++)
            		printf("%d: %d\n",k,withCL.vec[i].coeffs[k]);
        }
        //1) NTT for c-code
        timespec_get(&start_cpu, TIME_UTC);
        for (j = 0; j < Iters; ++j) {
            for (i = 0; i < polyNum; ++i) {
            	poly_ntt(&noCL.vec[i]);
            }
        }
        timespec_get(&end_cpu, TIME_UTC);

        timeCPU += ((end_cpu.tv_sec-start_cpu.tv_sec)*1000000000L) + (end_cpu.tv_nsec - start_cpu.tv_nsec);

        //2) NTT for CL-code
        time = 0;
        timespec_get(&start, TIME_UTC);
        for (j = 0; j < Iters; ++j) {
        	cl_event prof_event;
			cl_ulong cl_tstart, cl_tend;

			cl_mem inout_buff;
			size_t global_size = N * polyNum;
			//---------------------------------------------------------------------------------
			//Create Buffers

			inout_buff = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint) * global_size, &withCL, &err);
			if(DEBUG && err<0)perror("Error in input buffer creation");

			if(DEBUG)printf("Setting kernel arguments.\n");
			cl_uint tmp_rounds = (unsigned int)(log2(float(N)));
			if(DEBUG)printf("NTT Length=%d,rounds=%d\n",N,tmp_rounds);
			err |= clSetKernelArg(NTTkernel, 0, sizeof(cl_mem), &inout_buff);

			if(DEBUG && err <0)perror("Setting kernel arguments failed.\n");
//			err = clEnqueueNDRangeKernel(queue, NTTkernel, 1, NULL, &global_size, &work_units_per_kernel, 0, NULL, &prof_event);
			err = clEnqueueTask(queue,NTTkernel,0,NULL,&prof_event);
			//without profiler
//			err = clEnqueueNDRangeKernel(queue, NTTkernel, 1, NULL, &global_size, &work_units_per_kernel, 0, NULL, NULL);
			if(DEBUG && err<0)perror("Error in kernel execution");
			//---------------------------------------------------------------------------------
			//Launch Kernel
//			printf("Launching kernel.\n");
			err = clEnqueueReadBuffer(queue, inout_buff, CL_TRUE, 0, sizeof(cl_uint) * global_size, &withCL, 0, NULL, NULL);
			if(DEBUG && err<0)perror("Error in output buffer read");
			err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_START, sizeof(cl_tstart), &cl_tstart, NULL);
			err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_END, sizeof(cl_tend), &cl_tend, NULL);
			//---------------------------------------------------------------------------------
			//Release mem objects
			clReleaseMemObject(inout_buff);
//			clReleaseMemObject(out_buff);
			time = time + cl_tend - cl_tstart;
        }
        timespec_get(&end, TIME_UTC);
        timeOCL += ((end.tv_sec-start.tv_sec)*1000000000L) + (end.tv_nsec - start.tv_nsec);
        timeOCL_kernel += time;

        //---------------------------------------------------------------------------------
        //Compare results with reference
        for (i = 0; i < polyNum; ++i) {
        	for( int k =0 ; k<N ; k++){
        		if(noCL.vec[i].coeffs[k] != withCL.vec[i].coeffs[k]){
        			tv_missmatch = 1;
        			printf("%d: %d ~= %d\n",k,noCL.vec[i].coeffs[k],withCL.vec[i].coeffs[k]);
//        			break;
        		}
        		else
        			printf("%d: %d == %d\n",k,noCL.vec[i].coeffs[k],withCL.vec[i].coeffs[k]);
        	}
        }
    }
    if(tv_missmatch)printf("----------------------\nVectors missmatch between CL and No-CL run.\n----------------------\n");
    else printf("----------------------\nTEST PASSED.\n----------------------\n");
    printf("----------------------\nTotal Times\n----------------------\n");
    printf("\n%d CPU NTT operations took %ld nanonseconds\n----------------------\n", NTESTS * Iters, timeCPU);
    printf("\n%d OpenCL NTT_N operation took %ld nanonseconds\n----------------------\n", NTESTS * Iters, timeOCL);
    printf("\n%d OpenCL-kernel NTT_N operation took %ld nanonseconds\n----------------------\n", NTESTS * Iters, timeOCL_kernel);
    printf("----------------------\nAverage Times\n----------------------\n");
    printf("\nAverage CPU NTT operations took %ld nanonseconds\n----------------------\n", timeCPU/(NTESTS * Iters));
    printf("\nAverage OpenCL NTT_N operation took %ld nanonseconds\n----------------------\n", timeOCL/(NTESTS * Iters));
    printf("\nAverage OpenCL-kernel NTT_N operation took %ld nanonseconds\n----------------------\n", timeOCL_kernel/(NTESTS * Iters));
    //---------------------------------------------------------------------------------
    //Release CL objects
//    printf("Releasing OpenCL objects.\n");
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
	clReleaseDevice(device_id);
	clReleaseKernel(NTTkernel);
	clReleaseProgram(program);
	free(platform_ids);
    return 0;
}

int load_file_to_memory(const char *filename, char **result) {
    uint size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL) {
        *result = NULL;
        return -1; // -1 means file opening fail
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *)malloc(size+1);
    if (size != fread(*result, sizeof(char), size, f)) {
        free(*result);
        return -2; // -2 means file reading fail
    }
    fclose(f);
    (*result)[size] = 0;
    return size;
}
