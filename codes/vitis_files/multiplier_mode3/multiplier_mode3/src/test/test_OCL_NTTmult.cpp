#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include "../params.h"			//Dilithium Parameters
#include "../randombytes.h"		//Randombytes function
#include "../polyvec.h"
#include "../poly.h"			//Declaration of poly functions and structs
#include "../ntt.h"
#include "../fips202.h"

//#include "xcl2.hpp"
#include "CL/cl.h"

#include <time.h>
#include <math.h>

//Running in parallel new interface for hw-calls
#include "../dilithium_mult_ocl.hpp"
#define N 256

void initialize_device(const char* kernel_file, const char* kernel_name, cl_context* context, cl_kernel* kernel, cl_command_queue* queue );

//Debug mode for printing
#define DEBUG 1

int main (int argc, char *argv[])
{
  //passing arguments from command line
  //example: ./pqClean_mode2 100
  int NTESTS;
  //Number of tests to run
  if(argc == 2){
	  NTESTS = atoi(argv[1]);
  }
  else{
	  NTESTS = 100;
  }

    struct timespec start, end, start_cpu, end_cpu;

	/*---------------------------------------------------------------------------------
					Run for 1) C-code and 2) HW-device
	---------------------------------------------------------------------------------*/
    cl_ulong timeOCL_kernel = 0;
    long int in_io_time,buf_aloc_time,migrate_in,migrate_out,out_io_time;
    long int sum_in_in_io_time =0;
    long int sum_buf_aloc_time=0;
    long int sum_migrate_in=0;
    long int sum_migrate_out=0;
    long int sum_out_io_time=0;
    long int timeCPU =0;
    long int timeOCL =0;
	polyveck hw_out;

    printf("\nSize of polynomials: K=%d,L=%d \n", K, L);

    int tv_missmatch = 0;
    for (int test = 0; test < NTESTS; ++test) {
    	// Create test-vectors
    	uint8_t seedbuf[2 * SEEDBYTES + CRHBYTES];
		const uint8_t *rho, *rhoprime;
		polyvecl mat[K], s1, s1hat, s1hat_hw;
		polyveck t1;

		/* Get randomness for rho, rhoprime and key */
		randombytes(seedbuf, SEEDBYTES);
		shake256(seedbuf, 2 * SEEDBYTES + CRHBYTES, seedbuf, SEEDBYTES);
		rho = seedbuf;
		rhoprime = rho + SEEDBYTES;
		/* Expand matrix */
		PQCLEAN_DILITHIUM3_CLEAN_polyvec_matrix_expand(mat, rho);
		/* Sample short vectors s1 and s2 */
		PQCLEAN_DILITHIUM3_CLEAN_polyvecl_uniform_eta(&s1, rhoprime, 0);
		s1hat = s1;
		//copy test-vectors to hardware variables
		for (int l = 0; l < L; l++) {
//			memcpy(&s1hat_hw.vec[l], &s1hat.vec[l], sizeof(uint32_t) * N);
			for(int i=0; i<N;i++){
				s1hat_hw.vec[l].coeffs[i] = s1hat.vec[l].coeffs[i];
			}
		}

		/* Matrix-vector multiplication */
		//1) NTT for c-code
		timespec_get(&start_cpu, TIME_UTC);
		PQCLEAN_DILITHIUM3_CLEAN_polyvecl_ntt(&s1hat);
		PQCLEAN_DILITHIUM3_CLEAN_polyvec_matrix_pointwise_montgomery(&t1, mat, &s1hat);
		PQCLEAN_DILITHIUM3_CLEAN_polyveck_reduce(&t1);
		PQCLEAN_DILITHIUM3_CLEAN_polyveck_invntt_tomont(&t1);
		timespec_get(&end_cpu, TIME_UTC);
		timeCPU += ((end_cpu.tv_sec-start_cpu.tv_sec)*1000000000L) + (end_cpu.tv_nsec - start_cpu.tv_nsec);

        //2) NTT for HW-code
        timespec_get(&start, TIME_UTC);
        timeOCL_kernel += dilithium_mult_ocl(&s1hat_hw,mat,&hw_out,&in_io_time,&buf_aloc_time,
        		&migrate_in,&migrate_out,&out_io_time);
        timespec_get(&end, TIME_UTC);

		// Update timing-report
        if(test == 0)	//calculated once per kernel-setup
        	sum_buf_aloc_time = buf_aloc_time;
        else{			//calculated for all other frames
        	timeOCL += ((end.tv_sec-start.tv_sec)*1000000000L) + (end.tv_nsec - start.tv_nsec);
        	sum_in_in_io_time += in_io_time;
        	sum_migrate_in += migrate_in;
        	sum_migrate_out += migrate_out;
        	sum_out_io_time += out_io_time;
        }

        //Compare results with reference
        if(test != 0){	//bypass setup-frame
			for (int k = 0; k < K; k++) {
				for( int i =0 ; i<N ; i++){
					//compare HW-SW results
					if(t1.vec[k].coeffs[i] != hw_out.vec[k].coeffs[i]){
						if(DEBUG){	//printing t1
							printf("[%d,%d]: %d ~= %d\n",k,i,t1.vec[k].coeffs[i],hw_out.vec[k].coeffs[i]); //comp with _c
						}
						tv_missmatch = 1;
					}
				}
			}
        }
    }

    if(tv_missmatch)printf("----------------------\nVectors missmatch between CL and No-CL run.\n----------------------\n");
    else printf("----------------------\nTEST PASSED.\n----------------------\n");
    printf("----------------------\nTotal Times\n----------------------\n");
    printf("\n%d CPU Dilithium-NTT operations took %ld nanonseconds\n----------------------\n", NTESTS, timeCPU);
    printf("\n%d SW/HW execution with device-initialization%ld nanonseconds\n----------------------\n", NTESTS-1, timeOCL);
    printf("\n%d HW-kernel Dilithium-NTT operation took %ld nanonseconds\n----------------------\n", NTESTS, timeOCL_kernel);

    printf("\n%d Input copy operations operations took %ld nanonseconds\n----------------------\n", NTESTS-1, sum_in_in_io_time);
    printf("\n%d Input buffer-copies operations took %ld nanonseconds\n----------------------\n", 1, sum_buf_aloc_time);
    printf("\n%d Enqueue Migrate Objects IN operations took %ld nanonseconds\n----------------------\n", NTESTS-1, sum_migrate_in);
    printf("\n%d Enqueue Migrate Objects OUT operations took %ld nanonseconds\n----------------------\n", NTESTS-1, sum_migrate_out);
    printf("\n%d Output copy operations operation took %ld nanonseconds\n----------------------\n", NTESTS-1, sum_out_io_time);

    printf("\n\n----------------------\nAverage Times\n----------------------\n");
    printf("\nAverage CPU Dilithium-NTT operations took %ld nanonseconds\n----------------------\n", timeCPU/(NTESTS));
    printf("\nAverage SW/HW execution with device-initialization%ld nanonseconds\n----------------------\n", timeOCL/(NTESTS-1));
    printf("\nAverage HW-kernel Dilithium-NTT operation took %ld nanonseconds\n----------------------\n", timeOCL_kernel/(NTESTS));

    printf("\nAverage Input copy operations took %ld nanonseconds\n----------------------\n", sum_in_in_io_time/(NTESTS-1));
    printf("\n1 Input buffer-allocation operation took %ld nanonseconds\n----------------------\n", sum_buf_aloc_time);
    printf("\nAverage Enqueue Migrate Objects IN operations took %ld nanonseconds\n----------------------\n", sum_migrate_in/(NTESTS-1));
    printf("\nAverage Enqueue Migrate Objects OUT operations took %ld nanonseconds\n----------------------\n", sum_migrate_out/(NTESTS-1));
    printf("\nAverage Output copy operations operation took %ld nanonseconds\n----------------------\n", sum_out_io_time/(NTESTS-1));
    return 0;
}
